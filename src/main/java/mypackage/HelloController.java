package mypackage;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
    // CRUD = Create, Read, Update, Delete
//    @RequestMapping("/hello")
    // better than request mapping
    @GetMapping("/hello")
    public String getHelloMessage() {
        return "Hello World!!!";
    }


}
